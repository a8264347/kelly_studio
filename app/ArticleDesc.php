<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ArticleDesc extends Model
{
	protected $primaryKey = null;
	public $incrementing = false;
	protected $table = 'article_desc';

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
}
