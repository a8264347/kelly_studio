<?php

namespace App\Services;

use App\Repositories\ProductMenuRepository;
use App\Repositories\ProductRepository;

class ProductService
{
	protected $product_menu;
    protected $product;

    // 透過 DI 注入 Repository
    public function __construct(ProductMenuRepository $product_menu, ProductRepository $product)
    {
        $this->product_menu = $product_menu;
        $this->product = $product;
    }

    public function getProductMenuWithProduct($id)
    {
        return $this->product_menu->productCategory($id);
    }

    public function getProductMenuWithProductPrice($id,$price = 0,$limit = 1,$order = '',$mark =''){
        $result_product = $this->product_menu->productCategoryWithCondition($id,$price,$limit,$order,$mark);
        return [
            'product_menu' => $result_product
        ];
    }

    public function getProductAll()
    {
        return [
            'product_menu' => $this->product_menu->productAll()
        ];
    }

    public function getProductAllWithPage($limit = 1)
    {
        return [
            'product_menu' => $this->product_menu->productAllWithPage($limit)
        ];
    }

    public function processIntPrice($price_arr){
        return array_map(function ($price) {
            return (int)$price;
        }, $price_arr);
    }

    public function processStore($type,$data){
        $product = $type === 'product' ? $this->product->createRecord($data) : $this->product_menu->createRecord($data);
        return $product;
    }

    public function processUpdate($type,$id,$data){
        $product = $type === 'product' ? $this->product->updateRecord($id, $data) : $this->product_menu->updateRecord($id, $data);
        return $product;
    }

    public function processDelete($type,$id){
        $result = $type === 'product' ? $this->product->findRecord($id) : $this->product_menu->findRecord($id);
        $result->delete();
        return $result;
    }

    public function delteRestore($type,$id)
    {
        $result = $type === 'product' ? $this->product->RestoreRecord($id) : $this->product_menu->RestoreRecord($id);
        return $result;
    }
}
