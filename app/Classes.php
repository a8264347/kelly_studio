<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Classes extends Model
{
    protected $table = 'classes';
    public static $showField = [
        'name',
        'name_e',
        'classes_type',
        'price',
        'photo',
        'introduction',
        'introduction_e',
        'content',
        'content_e',
        'speaker_id',
        'link_fb'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
}
