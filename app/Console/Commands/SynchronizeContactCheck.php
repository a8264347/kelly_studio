<?php

namespace App\Console\Commands;

use App\BotMessage;
use Illuminate\Console\Command;
use App\Contact;

class SynchronizeContactCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kellystudio:syn-contact';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步 Contact 已確認的報名事項';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fh = fopen('/tmp/Synchronize.txt','r');
        // define an eampty array
        $data = array();
        // read data
        while ($line = fgets($fh)) {
            // if the line has some data
           if(trim($line)!=''){
               // explode each line data
               $line_data = explode('-',$line);
               if (!empty($line_data)) {
                   foreach ($line_data as $key => $value) {
                        $contact = Contact::where('uuid', $value)->update(array('status' => 1));
                   }
               }
           }
        }
        fclose($fh);
    }
}
