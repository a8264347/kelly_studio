<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Customer extends Model
{
	public static $showField = [
        'name',
	    'name_e',
	    'job_title',
	    'job_title_e',
	    'photo',
	    'introduction',
	    'introduction_e',
	    'message',
	    'class_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
}
