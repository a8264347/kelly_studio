<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ApplicationAuth extends Model
{
    protected $table = 'application_auth';
    protected $fillable = [
        'company_name', 'company_name_e', 'username','password','email','ip','url'
    ];
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
}
