<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Warning extends Mailable
{
    use Queueable, SerializesModels;
    public $params;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    public function build()
    {
        // 透過 with 把參數指定給 view
        return $this->subject("處理新報名事宜")
            ->view('emails.warning')
            ->with([
                'params' => $this->params,
            ]);
    }
}
