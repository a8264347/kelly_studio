<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Mail\Warning;
use Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $details;
    public $params;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details,$params)
    {
        $this->details = $details;
        $this->params  = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // 收件者務必使用 collect 指定二維陣列，每個項目務必包含 "name", "email"
        $to = collect([
            ['name' => $this->details['name'], 'email' => $this->details['email']]
        ]);

        Mail::to($to)->cc(['a8264347@gmail.com'])->send(new Warning($this->params));
    }
}
