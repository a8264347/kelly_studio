<?php

namespace App\Repositories;

use App\Product;
use App\Contracts\RepositoryAbstract;
/**
 * Class ProductRepository.
 */
class ProductRepository extends RepositoryAbstract
{
    protected $product;

    // 透過 DI 注入 Model
    public function __construct(Product $product)
    {
        $this->product = $product;
        parent::__construct();
    }

    public function modelUse(){
        return Product::class;
    }

    // 取資料邏輯：取得價錢與分頁
    public function getRangeRprice($price = 0,$limit = 1)
    {
        return $this->product
            ->where('price', $price)
            ->paginate($limit);
    }

    // 取資料邏輯：取得排序價錢(大於)
    public function getRangeRpriceOrder($price = 0,$limit = 1,$order = 'asc',$mark = 'greater')
    {
        $mark  = $mark === 'greater' ? '>=' : '<=' ;
        $order = $order === 'asc' ? 'asc' : 'desc' ;
        return $this->product
            ->where('price', $mark, $price)
            ->orderBy('price',$order)
            ->paginate($limit);
    }
}
