<?php

namespace App\Repositories;

use App\ProductMenu;
use App\Contracts\RepositoryAbstract;
/**
 * Class ProductMenuRepository.
 */
class ProductMenuRepository extends RepositoryAbstract
{
	protected $product_menu;

    // 透過 DI 注入 Model
    public function __construct(ProductMenu $product_menu)
    {
        $this->product_menu = $product_menu;
        parent::__construct();
    }

    public function modelUse(){
        return ProductMenu::class;
    }

    public function getAllMenu($order = 'asc'){
        $order = $order === 'asc' ? 'asc' : 'desc' ;
        return $this->product_menu
            ->orderBy('id',$order)
            ->get();
    }

    public function productCategory($id){
        $product_category = ProductMenu::with('product')->find($id);
        return $product_category;
    }

    public function productCategoryWithCondition($id,$price = 0,$limit = 1,$order = 'asc',$mark = 'less')
	{
        $mark  = $mark === 'less' ? '<=' : '>=' ;
        $order = $order === 'desc' ? 'desc' : 'asc' ;
        $con_arr = array(
            "mark"  => $mark,
            "order" => $order,
            "limit" => (int)$limit,
            "price" => (int)$price
        );
        $product_category = ProductMenu::with(['product' => function ($query) use ($con_arr) {
            $query->where('price', $con_arr['mark'], $con_arr['price'])
                    ->orderBy('price',$con_arr['order'])
                    ->paginate($con_arr['limit']);
        }])->find($id);

	    return $product_category;
	}

    public function productAll()
    {
        $product_category = ProductMenu::with('product')->get();
        return $product_category;
    }

    public function productAllWithPage($limit = 1)
    {
        $product_category = ProductMenu::with('product')->paginate($limit);
        return $product_category;
    }

}
