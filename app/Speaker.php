<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Speaker extends Model
{
	public static $photoPath = '/images/speaker';
	public static $showField = [
        'name',
        'name_e',
        'job_title',
        'job_title_e',
        'speakers_type',
        'photo',
        'introduction',
        'introduction_e',
        'content',
        'content_e',
        'link_fb'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
}
