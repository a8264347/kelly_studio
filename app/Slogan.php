<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Slogan extends Model
{
	public static $showField = [
        'title',
        'title_e',
        'short_introduction',
        'short_introduction_e',
        'mark_class',
        'content',
        'content_e'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
}
