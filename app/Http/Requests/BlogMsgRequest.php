<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class BlogMsgRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'blog_id' => 'nullable|integer|min:0|max:20',
            'content' => 'nullable|string',
            'content_e' => 'nullable|string',
            'created_by' => 'nullable|integer|min:0|max:20',
            'updated_by' => 'nullable|integer|min:0|max:20'
        ];
    }
}
