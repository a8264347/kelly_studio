<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplicationAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name'   => 'required|string|max:50',
            'company_name_e' => 'required|string|max:50',
            'username'  => 'required|string|max:50',
            'password'  => 'required|string',
            'email'     => 'required|string',
            'ip'        => 'required|string',
            'url'       => 'required|string',
            'accept_by' => 'required|integer|min:0|max:20'
        ];
    }
}
