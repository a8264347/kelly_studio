<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:36',
            'name_e' => 'nullable|string|max:100',
            'job_title' => 'nullable|string|max:20',
            'job_title_e' => 'nullable|string|max:20',
            'photo' => 'nullable|string|max:100',
            'introduction' => 'nullable|string|max:100',
            'introduction_e' => 'nullable|string|max:100',
            'message' => 'nullable|string',
            'class_id' => 'nullable|integer|min:0|max:20',
            'created_by' => 'nullable|integer|min:0|max:20',
            'updated_by' => 'nullable|integer|min:0|max:20'
        ];
    }
}
