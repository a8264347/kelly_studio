<?php

namespace App\Http\Requests;

use App\Speaker;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class SpeakerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'name_e' => 'nullable|string|max:100',
            'job_title' => 'nullable|string|max:20',
            'job_title_e' => 'nullable|string|max:20',
            'type' => 'nullable|integer|min:0|max:20',
            'photo' => 'nullable|string|max:100',
            'introduction' => 'nullable|string|max:100',
            'introduction_e' => 'nullable|string|max:100',
            'content' => 'nullable|string',
            'content_e' => 'nullable|string',
            'link_fb' => 'nullable|url',
            'created_by' => 'nullable|integer|min:0|max:20',
            'updated_by' => 'nullable|integer|min:0|max:20'
        ];
    }
}
