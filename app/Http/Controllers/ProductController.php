<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\ProductService;
use Illuminate\Http\UploadedFile;
use Storage;

class ProductController extends Controller
{
    use ApiTrait;

    protected $product_service;

    //透過 DI 注入 Service
    public function __construct(ProductService $product_service)
    {
        $id = isset(auth()->user()->id)?auth()->user()->id:0;
        $this->middleware('log.studio:'.$id);
        $this->product_service = $product_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 直接呼叫 Service 包裝好的 method
        return $this->returnSuccess('Success.', $this->product_service->getProductAll());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('file')) {
            $url_return = $this->savePhoto($request->file('photo'));
        }
        $type = $request->input('type', 'product');
        $data = $request->only(['name', 'name_e','photo','introduction','introduction_e','price']);

        $product = $this->product_service->processStore($type,$data);
        return $this->returnSuccess('Store success.', $product);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->returnSuccess('Success.', $this->product_service->getProductMenuWithProduct($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->hasFile('file')) {
            $url_return = $this->savePhoto($request->file('photo'));
        }
        $type = $request->input('type', 'product');
        $data = $request->only(['name', 'name_e','photo','introduction','introduction_e','price']);

        $product = $this->product_service->processUpdate($type,$id,$data);
        return $this->returnSuccess('Update success.', $product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $type = $request->input('type', 'product');
        $this->product_service->processDelete($type,$id);
        return $this->returnSuccess('destroy success.');
    }

    public function showId(Request $request,$id)
    {
        $price = $request->input('price', 0);
        $limit = $request->input('limit', 1);
        $order = $request->input('order', 'asc');
        $mark  = $request->input('mark', 'greater');
        return $this->returnSuccess('Success.', $this->product_service->getProductMenuWithProductPrice($id,$price,$limit,$order,$mark));
    }

    public function productRestore(Request $request,$id)
    {
        $type = $request->input('type', 'product');
        $this->product_service->delteRestore($type,$id);
        return $this->returnSuccess('restore success.');
    }

    public function savePhoto(UploadedFile $image){
        $file_name = auth()->user()->id . '-'  . Str::random(8). '-' . $image->getClientOriginalName();
        Storage::put($file_name, $image->get());
        return 'storage/'.$file_name;
    }
}
