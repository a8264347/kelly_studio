<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexBlogController extends Controller
{
	public function index()
	{
    	return view('frontend.blog');
	}
}
