<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogMsg;
use Illuminate\Http\Request;
use App\Http\Requests\BlogRequest;
use Session;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blog::orderBy('created_at','desc')->take(3)->get();
        $data = [
            'blog' => $blog->only(Blog::$showField),
        ];
        return $this->returnSuccess('Success.', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function show(string $uuid)
    {
        $blog = Blog::where('uuid', '=', $uuid)->firstOrFail();
        $data = [
            'blog' => $blog->only(Blog::$showField),
        ];
        return $this->returnSuccess('Success.', $data);
    }
    /**
     * Display the specified resource.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function showBlogsById(string $uuid)
    {
        $locale  = Session::get('locale', 'en');
        $blog    = Blog::where('uuid', '=', $uuid)->firstOrFail()->toArray();
        $blogs   = Blog::where('id', '!=', $blog['id'])->orderBy('id','asc')->get()->random(3);
        $collection    = BlogMsg::where('blog_id', '=', $blog['id'])->orderBy('blog_id','asc')->get();
        $desc          = array();
        if (!$collection->isEmpty()) {
            $desc = $collection->random(3);
        }
        if ($locale === 'en') {
            $blog['title'] = $blog['title_e'];
            $blog['short_introduction'] = $blog['short_introduction_e'];
            $blog['content']   = $blog['content_e'];
            foreach ($blogs as $key => $value) {
                $blogs[$key]['title'] = $value['title_e'];
                $blogs[$key]['short_introduction'] = $value['short_introduction_e'];
                $blogs[$key]['content']   = $value['content_e'];
            }
        }
        $blog_desc_arr = array(
            'blog'      => $blog,
            'blogs'     => $blogs,
            'blog_desc' => $desc,
            'blog_block_count' => count($blogs)
        );
        return view('frontend.blog_single',$blog_desc_arr);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
