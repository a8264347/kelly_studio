<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\Warning;

class WarningController extends Controller
{
    public function sendMail($name,$email,$phone,$remark,$class)
    {
        // 收件者務必使用 collect 指定二維陣列，每個項目務必包含 "name", "email"
        $to = collect([
            ['name' => $name, 'email' => $email]
        ]);

        // 提供給模板的參數
        $params = [
            'say' => '您好，有一段新報名事宜 <br/>
                        報名人:'.$name.'<br/>
                        信箱:'.$email.'<br/>
                        電話:'.$phone.'<br/>
                        備註:'.$remark.'<br/>
                        興趣課程:'.$class
        ];

        // 若要直接檢視模板
        // echo (new Warning($data))->render();die;

        Mail::to($to)->cc(['a8264347@gmail.com'])->send(new Warning($params));
        if(count(Mail::failures()) > 0){
            return false;
        }
        return true;
    }
}
