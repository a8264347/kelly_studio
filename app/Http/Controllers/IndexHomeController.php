<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use App\Speaker;
use App\Article;
use App\Blog;
use App\Customer;
use App\Slogan;
use App\Classes;

class IndexHomeController extends Controller
{
	public function index()
	{
		$locale = Session::get('locale', '');
		if (!in_array($locale, ['tw', 'en'])) {
            $locale = 'tw';
        }
        App::setLocale($locale);
        session()->put('locale', $locale);
		$data['speakers'] = Speaker::orderByRaw("RAND()")->get()->toArray();
		$data['articles'] = Article::orderBy('created_at','desc')->get()->toArray();
		$article_count = count($data['articles']);
		$blogs = Blog::orderBy('created_at','desc')->get()->random(3);
		//default(articles_block)
		$data['articles_block'] = 1;
		if (!empty($blogs)) {
			foreach ($blogs as $key => $value) {
				$blogs[$key]['blog_owner']  = 'Edmond';
				// $blogs[$key]['blog_commit'] = Http::get('/blog_msg/'.$value['id']);
			}
		}
		$data['blogs'] = $blogs;
		$data['customers'] = Customer::orderBy('created_at','desc')->get()->random(3);
		$data['visions'] = Slogan::orderBy('created_at','desc')->take(3)->get();
		$data['classes'] = Classes::orderBy('created_at','desc')->get()->random(4);
		if ($locale === 'en') {
			foreach ($data['speakers'] as $key => $value) {
				$data['speakers'][$key]['job_title'] = Controller::$job_title_config['job_title_e'][$value['job_title_e']];
				$data['speakers'][$key]['name'] = $value['name_e'];
				$data['speakers'][$key]['link'] = '';
				continue;
			}
			foreach ($data['customers'] as $key => $value) {
				$data['customers'][$key]['job_title'] = $value['job_title_e'];
				$data['customers'][$key]['name'] = $value['name_e'];
				continue;
			}
			foreach ($data['articles'] as $key => $value) {
				$data['articles'][$key]['title'] = $value['title_e'];
				$data['articles'][$key]['link']  = '';
				continue;
			}
			foreach ($data['classes'] as $key => $value) {
				$data['classes'][$key]['name']  = $value['name_e'];
				$data['classes'][$key]['content']      = $value['content_e'];
				$data['classes'][$key]['introduction'] = $value['introduction_e'];
				$data['classes'][$key]['link']  = '';
				continue;
			}
			foreach ($data['visions'] as $key => $value) {
				$data['visions'][$key]['title']               = $value['title_e'];
				$data['visions'][$key]['short_introduction']  = $value['short_introduction_e'];
				continue;
			}
		}
		else{
			foreach ($data['speakers'] as $key => $value) {
				$data['speakers'][$key]['job_title'] = Controller::$job_title_config['job_title'][$value['job_title']];
				continue;
			}
		}
		if ($article_count > 1) {
			$data['articles_block'] = (int)($article_count/8)+1;
			$data_articles = array();
			for ($i=1; $i <= $data['articles_block']; $i++) {
				$limit = $i-1;
				foreach ($data['articles'] as $key => $value) {
					if (($key+1)/8 <= $i && ($key+1)/8 > $limit) {
						$data_articles[$i][$key] = $value;
					}
				}
			}
			$data['articles'] = $data_articles;
		}
		$data['page_index'] = 'active';
		return view('frontend.index',$data);
	}
}
