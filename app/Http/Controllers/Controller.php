<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public static $job_title_config = array(
        'job_title' => array(
            0 => '講師',
            1 => '外聘講師',
            2 => '助理',
            3 => '執行長'
        ),
        'job_title_e' =>   array(
            0 => 'Lecturer',
            1 => 'Visiting Lecturer',
            2 => 'assistant',
            3 => 'ceo'
        )
    );
}
