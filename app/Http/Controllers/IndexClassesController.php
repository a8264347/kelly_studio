<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Classes;

class IndexClassesController extends Controller
{
	public function index()
	{
		$locale = Session::get('locale', 'en');
		$data['classes_block_count'] = 1;
		$classes_count = count(Classes::orderBy('created_at','desc')->take(4)->get());
		$data['classes'] = Classes::orderBy('created_at','desc')->get();
		if ($classes_count > 1) {
			$data['classes_block_count'] = (int)($classes_count/8)+1;
		}
		if ($locale === 'en') {
			foreach ($data['classes'] as $key => $value) {
				$data['classes'][$key]['title'] = $value['title_e'];
				$data['classes'][$key]['introduction'] = $value['introduction_e'];
				$data['classes'][$key]['link']  = '';
				continue;
			}
		}
		$data['page_classes'] = 'active';
		return view('frontend.classes',$data);
	}
	public function show($uuid)
    {
    	$locale = Session::get('locale', 'en');
    	$data   = array();
    	$data['classes_block_count'] = 1;
        $class  = Classes::where('uuid', '=', $uuid)->firstOrFail();
        $class  = $class->toArray();
        $classes_count   = count(Classes::orderBy('created_at','desc')->take(4)->get());
        if ($classes_count > 1) {
			$data['classes_block_count'] = (int)($classes_count/8)+1;
		}
		$data['class']   = $class;
        $data['classes'] = Classes::select("*")
				                ->where("uuid",'!=', $uuid)
				                ->orderBy("created_at", "desc")
				                ->take(4)->get();
		if ($locale === 'en') {
			$data['class']['name']    = $data['class']['name_e'];
			$data['class']['content'] = $data['class']['content_e'];
			$data['class']['introduction'] = $data['class']['introduction_e'];

			foreach ($data['classes'] as $key => $value) {
				$data['classes'][$key]['name'] = $value['name_e'];
				$data['classes'][$key]['content'] = $value['content_e'];
				$data['classes'][$key]['introduction'] = $value['introduction_e'];
			}
		}
        return view('frontend.classes_single',$data);
    }
}
