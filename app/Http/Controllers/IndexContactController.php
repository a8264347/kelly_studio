<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use App\Rules\GoogleRecapchaV3Case;
use App\Contact;
use App\Classes;
use App\Http\Requests\ContactRequest;
use App\Http\Controllers\WarningController;
use Illuminate\Contracts\Validation\Validator;

class IndexContactController extends Controller
{
	public function index()
	{
        $locale = Session::get('locale', 'en');
		$data = array();
		$data['page_contact'] = 'active';
        $classes = Classes::all()->toArray();
        if (!empty($classes)) {
            foreach ($classes as $key => $value) {
                $name = $value['name'];
                if ($locale === 'en') {
                   $name = $value['name_e'];
                }
                $classes[$key]['name'] = $name;
            }
        }
        $data['classes'] = $classes;
		return view('frontend.contact',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function formStore(ContactRequest $request)
    {
        $uuid = $request->input('class_id', 0);
        $columns   = Classes::where('uuid', '=', $uuid)->first()->toArray();
        $class_id  = isset($columns['id'])?$columns['id']:0;
        $request->merge( array( 'class_id' => $class_id ) );
        $return = array(
            'error' => true,
            'msg'   => ''
        );
        $valid  = $this->validateForm($request);
        $data   = $request->only(['name', 'email','phone','message','class_id']);
        $data['phone']   = $request->input('phone', '');
        $data['message'] = $request->input('message', '');
        $data['status']  = 0;
        $contact = Contact::create($data);
        $name  = $request->input('name', 'edmond');
        $email = $request->input('email', 'a8264347@gmail.com');
        $class = isset($columns['name'])?$columns['name']:'無';
        $details = ['name' => $name, 'email' => $email];
        // 提供給模板的參數
        $params = array();
        $params['title']     = "您好，有一段新報名事宜";
        $params['content'][] = "報名人: $name";
        $params['content'][] = "信箱: $email";
        $params['content'][] = "電話: ".$data['phone'];
        $params['content'][] = "備註: ".$data['message'];
        $params['content'][] = "興趣課程: $class";

        dispatch(new \App\Jobs\SendEmailJob($details,$params));

        $return['error'] = false;
        return json_encode($return);
    }

    protected function validateForm(Request $request)
    {
        return $request->validate([
            'name'     => 'required|string',
            'email'    => 'required|email',
            'phone'    => 'nullable|string',
            'message'  => 'nullable|string',
            'class_id' => 'nullable|integer|min:0|max:20',
            'google_recaptcha_token' => ['required', 'string', new GoogleRecapchaV3Case()],
        ]);
    }
}
