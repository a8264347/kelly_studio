<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Speaker;
use App\Customer;

class IndexAboutController extends Controller
{
    public function index()
	{
		$locale = Session::get('locale', 'en');
		$data['speakers'] = Speaker::all();
		$data['customers'] = Customer::orderBy('created_at','desc')->take(3)->get();
		if ($locale == 'en') {
			foreach ($data['speakers'] as $key => $value) {
				$data['speakers'][$key]['job_title'] = Controller::$job_title_config['job_title_e'][$value['job_title_e']];
				$data['speakers'][$key]['name'] = $value['name_e'];
				$data['speakers'][$key]['link'] = '';
				continue;
			}
			foreach ($data['customers'] as $key => $value) {
				$data['customers'][$key]['job_title'] = $value['job_title_e'];
				$data['customers'][$key]['name'] = $value['name_e'];
				continue;
			}
		}
		else{
			foreach ($data['speakers'] as $key => $value) {
				$data['speakers'][$key]['job_title'] = Controller::$job_title_config['job_title'][$value['job_title']];
				continue;
			}
		}
		$data['instructors'] = 0;
		$data['views'] = 0;
		$data['students'] = 0;
		$data['statisfication'] = 0;
		$data['page_about'] = 'active';
		return view('frontend.about',$data);
	}
}
