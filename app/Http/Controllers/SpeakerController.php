<?php

namespace App\Http\Controllers;

use App\Speaker;
use Illuminate\Http\Request;
use App\Http\Requests\SpeakerRequest;
use Session;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Illuminate\Http\JsonResponse;

class SpeakerController extends Controller
{
    use ApiTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $speaker = Speaker::all();
        $data = [
            'speaker' => $speaker->only(Speaker::$showField),
        ];
        return $this->returnSuccess('Success.', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show(string $uuid)
    {
        $speaker = Speaker::where('uuid', '=', $uuid)->firstOrFail();
        $data = [
            'speaker' => $speaker->only(Speaker::$showField),
        ];
        return $this->returnSuccess('Success.', $data);
    }
    /**
     * Display the specified resource.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function showSpeakersById(string $uuid)
    {
        $locale     = Session::get('locale', 'en');
        $speaker    = Speaker::where('uuid', '=', $uuid)->firstOrFail()->toArray();
        $recommend  = Speaker::where('id', '!=', $speaker['id'])->orderBy('id','asc')->take(3)->get();
        if ($locale === 'en') {
            $speaker['job_title'] = Controller::$job_title_config['job_title_e'][$speaker['job_title_e']];
            $speaker['introduction'] = $speaker['introduction_e'];
            $speaker['content']   = $speaker['content_e'];
            $speaker['name']      = $speaker['name_e'];
            foreach ($recommend as $key => $value) {
                $recommend[$key]['job_title'] = Controller::$job_title_config['job_title_e'][$value['job_title_e']];
                $recommend[$key]['introduction'] = $value['introduction_e'];
                $recommend[$key]['content']   = $value['content_e'];
                $recommend[$key]['name']      = $value['name_e'];
            }
        }
        else{
            $speaker['job_title'] = Controller::$job_title_config['job_title'][$speaker['job_title']];
            foreach ($recommend as $key => $value) {
                $recommend[$key]['job_title'] = Controller::$job_title_config['job_title'][$value['job_title']];
            }
        }
        $speaker_recommend_arr = array(
            'speaker'       => $speaker,
            'recommend'     => $recommend,
            'speaker_block_count' => count($recommend)
        );
        return view('frontend.speaker_single',$speaker_recommend_arr);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UploadedFile  $image
     * @param  Speaker  $speaker
     * @return Speaker
     */
    public function savePhoto(UploadedFile $image, Speaker $speaker)
    {
        $newFileName = $speaker->name . '-' . Str::random(8) . '.' . $image->getClientOriginalExtension();
        $image->move(public_path(Speaker::$photoPath), $newFileName);

        $speaker->photo = url(Speaker::$photoPath . '/' . $newFileName);
        $speaker->save();

        return $speaker;
    }
}
