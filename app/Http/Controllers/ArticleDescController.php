<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleDesc;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleDescRequest;
use Session;

class ArticleDescController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show(string $uuid)
    {
        $article_desc = ArticleDesc::where('uuid', '=', $uuid)->firstOrFail();
        return $article_desc;
    }
    /**
     * Display the specified resource.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function showArticlesById(string $uuid)
    {
        $locale  = Session::get('locale', '');
        $article = Article::where('uuid', '=', $uuid)->firstOrFail()->toArray();
        $desc    = ArticleDesc::where('article_id', '=', $article['id'])->orderBy('id','asc')->get()->toArray();
        if ($locale === 'en') {
            $article['title']   = $article['title_e'];
            $article['content'] = $article['content_e'];
            $article['link']  = '';
            foreach ($desc as $key => $value) {
                $desc[$key]['content'] = $value['content_e'];
            }
        }
        $article_desc_arr = array(
        	'article'      => $article,
        	'article_desc' => $desc,
            'article_block_count' => count($desc)
        );

        foreach ($article_desc_arr['article_desc'] as $key => $value) {
            $article_desc_arr['article_desc'][$key]['type_first'] = empty($value['type']) ? 'display:none': '';
            $article_desc_arr['article_desc'][$key]['type_second'] = empty($value['type']) ? '': 'display:none';
        }
        return view('frontend.article_single',$article_desc_arr);
    }
}
