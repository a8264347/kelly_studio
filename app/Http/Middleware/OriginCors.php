<?php

namespace App\Http\Middleware;

use Closure;
use App\ApplicationAuth;

class OriginCors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tmp_application= ApplicationAuth::all();
        $allowedOrigins = array();
        foreach ($tmp_application as $key => $value) {
            $allowedOrigins[] = $tmp_application['ip'];
            $allowedOrigins[] = $tmp_application['url'];
        }
        $origin = $_SERVER['HTTP_ORIGIN'];
        if (in_array($origin, $allowedOrigins)) {
            return $next($request)
                ->header('Access-Control-Allow-Origin', $origin)
                ->header('Access-Control-Allow-Methods', 'GET, POST')
                ->header('Access-Control-Allow-Headers', 'Content-Type');
        }
    }
}
