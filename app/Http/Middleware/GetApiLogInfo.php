<?php

namespace App\Http\Middleware;

use Log;
use Request;
use Closure;

class GetApiLogInfo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$auth_name='')
    {
        Log::info('request access: ip = '.Request::ip().'，source url = '.$_SERVER['HTTP_HOST'].'auth_id:'.$auth_name);
        return $next($request);
    }
}
