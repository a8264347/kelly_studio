<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class ProductMenu extends Model
{
    use SoftDeletes;

    protected $table = 'product_menu';
    protected $dates = ['deleted_at'];

    public static $showField = [
        'name',
	    'name_e',
	    'photo'
    ];

    protected $fillable = [
        'name',
        'name_e',
        'photo'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }

    public function product()
    {
        return $this->hasMany('App\Product');
    }
}
