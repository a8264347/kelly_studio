<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Product extends Model
{
    use SoftDeletes;

	protected $table = 'product';
    protected $dates = ['deleted_at'];

    public static $showField = [
        'name',
	    'name_e',
	    'photo',
	    'introduction',
	    'introduction_e',
	    'price'
    ];

    protected $fillable = [
        'name',
        'name_e',
        'photo',
        'introduction',
        'introduction_e',
        'price'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }

    public function product_menu()
    {
        return $this->belongsTo('App\ProductMenu');
    }
}
