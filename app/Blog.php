<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Blog extends Model
{
	public static $showField = [
        'title',
        'title_e',
        'short_introduction',
        'short_introduction_e',
        'content',
        'content_e',
        'photo'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
}
