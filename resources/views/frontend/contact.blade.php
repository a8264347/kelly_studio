@extends('frontend.layouts.master')

@section('title',  __('contact.title'))

@section('content')
    <!-- inner banner -->
    <div class="inner-banner">
        <section class="w3l-breadcrumb">
            <div class="container">
                <ul class="breadcrumbs-custom-path">
                    <li><a href="{{ url('/') }}">{{ trans('contact.home') }}</a></li>
                    <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span> {{ trans('contact.contact_us') }}</li>
                </ul>
            </div>
        </section>
    </div>
    <!-- //inner banner -->
    <!-- contact -->
    <section class="w3l-contacts-12">
        <div class="contact-top pt-5">
            <div class="container py-md-4 py-3">
                <div class="title-heading-w3 text-center mx-auto">
                    <h3 class="title-main">{{ trans('contact.topic') }}</h3>
                    <p class="mt-4 sub-title"> {{ trans('contact.info') }}</p>
                </div>
                <div class="d-grid cont-main-top mt-5">
                    <!-- contact form -->
                    <div class="contacts12-main mt-lg-2">
                        <form id="mail_form" class="main-input">
                            <div class="top-inputs d-grid">
                                <input type="text" placeholder="{{ trans('contact.form_name') }}" name="name" id="w3lName" autocomplete="off" required="" value="{{ old('name') }}">
                                <input type="email" autocomplete="off" name="email" placeholder="{{ trans('contact.form_email') }}" id="w3lSender" required="" value="{{ old('email') }}">
                            </div>
                            <input type="text" autocomplete="off" placeholder="{{ trans('contact.form_phone') }}" name="phone" id="w3lPhone" required="" value="{{ old('phone') }}">
                            <textarea placeholder="{{ trans('contact.form_msg') }}" name="message" id="w3lMessage" required="">{{ old('message') }}</textarea>
                            <input type="hidden" name="google_recaptcha_token" id="ctl-recaptcha-token">
                            <!-- <div id="ctl-recaptcha-token"></div> -->
                            {{ csrf_field() }}
                            <select id="w3lPhone" name="class_id" class="form-control">
                                <option value="">{{ trans('contact.interest') }}</option>
                                @foreach ( $classes as $classes_key => $class )
                                <option value="{{ $class['uuid'] }}">{{ $class['name'] }}</option>
                                @endforeach
                            </select>
                            <button type="button" id="send_btn" class="btn btn-primary btn-style mt-4">{{ trans('contact.submit') }}</button>
                        </form>
                    </div>
                    <!-- //contact form -->
                    <!-- contact address -->
                    <div class="contact">
                        <div class="cont-subs">
                            <div class="cont-add">
                                <h4>{{ trans('contact.address_topic') }}:</h4>
                                <p class="contact-text-sub">{{ trans('contact.address') }}</p>
                            </div>
                            <div class="cont-add">
                                <h4>{{ trans('contact.address_email') }}:</h4>
                                <a href="#">
                                    <p class="contact-text-sub">{{ trans('contact.email') }}</p>
                                </a>
                            </div>
                            <div class="cont-add">
                                <h4>{{ trans('contact.address_phone') }}:</h4>
                                <a href="#">
                                    <p class="contact-text-sub">+{{ trans('contact.phone') }}</p>
                                </a>
                            </div>
                            <div class="social-icons-con">
                                <a href="{{config('app.fb_link')}}"><span class="fa fa-facebook-square" aria-hidden="true"></span></a>
                                <a href="{{config('app.ig_link')}}"><span class="fa fa-instagram" aria-hidden="true"></span></a>
                                <!-- <a href="#google-plus"><span class="fa fa-google-plus-square" aria-hidden="true"></span></a> -->
                            </div>
                        </div>
                    </div>
                    <!-- //contact address -->
                </div>
            </div>
            <!-- map -->
            <div class="map">
                <!-- X-Frame-Options: DENY -->
                <!-- https://stackoverflow.com/questions/41771125/laravel-refusing-to-display-in-iframe-as-x-frame-options-to-sameorigin -->
                <iframe src="{{config('app.google_map_link')}}" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
            <!-- //map -->
        </div>
    </section>
<!-- <script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit' async defer></script> -->
<script src="https://www.google.com/recaptcha/api.js?render={{env('RECAPTCHA_SITE_KEY')}}"></script>
<script type="text/javascript" defer="defer">
    grecaptcha.ready(function() {
        grecaptcha.execute('{{env('RECAPTCHA_SITE_KEY')}}').then(function(token) {
            document.getElementById('ctl-recaptcha-token').value = token;
        }).catch( error =>  console.log(error) );;
    });
    document.getElementById("send_btn").addEventListener("click",function(){
        swal({
            icon: "info",
            title: "{{ trans('contact_form.info') }}",
            buttons:{
                cancel: {
                    text: "{{ trans('contact_form.cancel') }}",
                    visible: true
                },
                confirm: {
                    text: "{{ trans('contact_form.confirm') }}",
                    visible: true
                }
            }
        }).then((visible) => {
            var data = new FormData(document.getElementById('mail_form'));
            data.set('_method', 'POST');
            axios.post("{{ secure_url('contact_form') }}",
                data).then((response) => {
                    if (!response.error) {
                        swal("{{ trans('contact_form.success') }}", "{{ trans('contact_form.success_msg') }}", "success");
                    } else {
                        swal("{{ trans('contact_form.warning') }}", '', "warning");
                    }
                }).catch((err) => {
                    var error = "{{ trans('contact_form.error_msg') }}";
                    if (err.response.data.errors['email']) {
                        error += ",{{ trans('contact_form.email') }}";
                    }
                    else if(err.response.data.errors['name']){
                        error += ",{{ trans('contact_form.name') }}";
                    }
                    else if(err.response.data.errors['phone']){
                        error += ",{{ trans('contact_form.phone') }}";
                    }
                    else if(err.response.data.errors['message']){
                        error += ",{{ trans('contact_form.message') }}";
                    }
                    else if(err.response.data.errors['class_id']){
                        error += ",{{ trans('contact_form.class_id') }}";
                    }
                    else if(err.response.data.errors['google_recaptcha_token']){
                        error += ",{{ trans('contact_form.google_recaptcha_token') }}";
                    }
                    swal("{{ trans('contact_form.error') }}", error, "error");
                    console.log("ERRRR:: ",err.response.data);
                })
        });
    });
</script>
@endsection