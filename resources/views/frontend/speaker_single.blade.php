<!-- {{ $last = $speaker_block_count }} -->

@extends('frontend.layouts.master')

@section('title', __('index.speaker_topic'))

@section('content')
<!-- inner banner -->
<div class="inner-banner">
  <section class="w3l-breadcrumb">
    <div class="container">
      <ul class="breadcrumbs-custom-path">
        <li><a href="{{ url('/') }}">{{ trans('index.speaker_topic') }}</a></li>
        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ $speaker['name'] }}</li>
      </ul>
    </div>
  </section>
</div>
<!-- //inner banner -->
<!-- classes-section -->
<section class="w3l-index-block2 py-5">
  <div class="container py-md-4 py-3">
    <div class="title-heading-w3 text-center mx-auto">
      <h3 class="title-main">{{ $speaker['name'] }}</h3>
      <img src="{{ asset($speaker['photo']) }}" alt="" class="img-fluid" />
      <h5 class="title-main">{{ $speaker['job_title'] }}</h3>
      <p style="">{!! nl2br(e($speaker['introduction']), false) !!}</p>
      <strong class="fee-class-w3 mt-3">{!! nl2br(e($speaker['content']), false) !!}</strong>
    </div>
    <br>
    <h3 class="title-main">{{ trans('index.speaker_professional') }}</h3>
    <div class="row bottom_grids mt-5 pt-lg-3">
      @foreach ( $recommend as $recommend_key => $recommend_val )
      <div class="col-lg-4 col-md-6 px-lg-4">
        <div class="s-block">
          <a href="{{ url('speaker') }}/{{ $recommend_val['uuid'] }}" class="d-block">
            <img src="{{ asset($recommend_val['photo']) }}" alt="" class="img-fluid-class" />
            <div class="p-3">
              <h3 class="mb-2">{{ $recommend_val['name'] }}</h3>
              <p>{{ $recommend_val['job_title'] }}</p>
            </div>
          </a>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>
@endsection