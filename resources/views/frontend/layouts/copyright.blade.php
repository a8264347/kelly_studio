<!-- copyright -->
<section class="w3l-copyright">
    <div class="container">
        <div class="row bottom-copies">
            <p class="col-lg-8 copy-footer-29">© 2020 Yogic. All rights reserved. Design by <a
                    href="https://w3layouts.com/" target="_blank" rel="noopener noreferrer">
                    W3Layouts</a></p>

            <div class="col-lg-4 footer-list-29 text-right">
                <div class="main-social-footer-29">
                    <!-- <a href="{{config('app.fb_link')}}" class="facebook"><span class="fa fa-facebook"></span></a> -->
                    <!-- <a href="{{config('app.ig_link')}}" class="instagram"><span class="fa fa-instagram"></span></a> -->
                </div>
            </div>
        </div>
    </div>
    <!-- move top -->
    <button onclick="topFunction()" id="movetop" title="Go to top">
        &#10548;
    </button>
</section>
<!-- //copyright -->