<!-- footer -->
<section class="w3l-footer-29-main">
    <div class="footer-29 py-5">
        <div class="container py-lg-4">
            <div class="row footer-top-29">
                <div class="col-lg-4 col-md-6 col-sm-7 footer-list-29 footer-1 pr-lg-5">
                    <div class="footer-logo mb-3">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img class="mid-img" src="{{ asset('images/logo.png') }}" alt="#">
                            <span>{{ trans('footer.hello_ceo') }}</span>
                        </a>
                    </div>
                    <p>{{ trans('footer.msg') }}</p>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-5 col-6 footer-list-29 footer-2 mt-sm-0 mt-5">

                    <ul>
                        <h6 class="footer-title-29">{{ trans('footer.link_topic') }}</h6>
                        <li><a href="{{ url('about') }}">{{ trans('footer.about_us') }}</a></li>
                        <li><a href="{{ url('class') }}"> {{ trans('footer.classes') }}</a></li>
                        <li><a class="pass_load" href="javascript:void(0);" onclick="scrollBlog();"> {{ trans('footer.blog') }}</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-5 col-6 footer-list-29 footer-3 mt-lg-0 mt-5">
                    <h6 class="footer-title-29">{{ trans('footer.more_info') }}</h6>
                    <ul>
                        <li><a href="{{ url('privacy') }}">{{ trans('footer.privacy_policy') }}</a></li>
                        <li><a href="{{ url('contact') }}">{{ trans('footer.contact_us') }}</a></li>
                    </ul>

                </div>
                <div class="col-lg-4 col-md-6 col-sm-7 footer-list-29 footer-4 mt-lg-0 mt-5">
                    <h6 class="footer-title-29">{{ trans('footer.contact_info') }}</h6>
                    <p>{{ trans('footer.address') }}</p>
                    <p class="mb-2 mt-3"><i class="fa fa-phone mr-2" aria-hidden="true"></i><a
                            class="pass_load" href="javascript:void(0);">+{{ trans('footer.phone') }}</a></p>
                    <p> <i class="fa fa-envelope mr-2" aria-hidden="true"></i><a
                            class="pass_load" href="javascript:void(0);">{{ trans('footer.email') }}</a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //footer -->