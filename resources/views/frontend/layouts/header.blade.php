@php
    $locale = Session::get('locale', 'tw');
    if ($locale === 'tw') {
        $tw = 'active';
        $en = '';
    } else {
        $tw = '';
        $en = 'active';
    }
@endphp
<header id="site-header" class="fixed-top">
    <div class="container">
        <nav class="navbar navbar-expand-lg stroke px-0">
            <h1>
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img class="sm-img" src="{{ asset('images/logo.png') }}" alt="#">
                    <span id="header_hello_ceo">{{ trans('header.hello_ceo') }}</span>
                </a>
            </h1>
            <button class="navbar-toggler  collapsed bg-gradient" type="button" data-toggle="collapse"
                data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon fa icon-expand fa-bars"></span>
                <span class="navbar-toggler-icon fa icon-close fa-times"></span>
            </button>

            <div class="collapse navbar-collapse navigation_test" id="navbarTogglerDemo02">
                <ul class="navbar-nav mx-lg-auto header_menu">
                    <li class="nav-item {{ $page_index ?? '' }}">
                        <a class="nav-link" href="{{ url('/') }}">{{ trans('header.home') }}<span class="sr-only"></span></a>
                    </li>
                    <li class="nav-item {{ $page_about ?? '' }}">
                        <a class="nav-link" href="{{ url('about') }}">{{ trans('header.about_us') }}</a>
                    </li>
                    <li class="nav-item {{ $page_classes ?? '' }}">
                        <a class="nav-link" href="{{ url('class') }}">{{ trans('header.classes') }}</a>
                    </li>
                    <li class="nav-item {{ $page_contact ?? '' }}">
                        <a class="nav-link" href="{{ url('contact') }}">{{ trans('header.contact_us') }}</a>
                    </li>
                    <li class="nav-item mobile_lang_tw">
                        <a class="nav-link {{ $tw }}" href="{{ url('/lang/tw') }}">中文</a>
                        <!-- /lang/tw -->
                    </li>
                    <li class="nav-item mobile_lang_en">
                        <a class="nav-link {{ $en }}" href="{{ url('/lang/en') }}">English</a>
                        <!-- /lang/en -->
                    </li>
                </ul>
            </div>
            <!-- search button -->
            <div class="search-right">
                <!-- <a class="pass_load" href="#search" title="search"><span class="fa fa-search" aria-hidden="true"></span></a> -->
                <!-- search popup -->
                <div id="search" class="pop-overlay">
                    <div class="popup">
                        <form action="{{ url('search') }}" method="post" class="search-box">
                            <input type="search" autocomplete="off" placeholder="{{ trans('header.keyword') }}" name="search" required="required" autofocus="">
                            <button type="submit" class="btn"><span class="fa fa-search"
                                    aria-hidden="true"></span></button>
                        </form>
                    </div>
                    <a class="close pass_load" href="#close">×</a>
                </div>
                <!-- //search popup -->
            </div>
            <!-- //search button -->
            <!-- toggle switch for light and dark theme -->
            <div class="cont-ser-position">
                <nav class="navigation">
                    <div class="theme-switch-wrapper">
                        <label class="theme-switch" for="checkbox">
                            <input type="checkbox" id="checkbox">
                            <div class="mode-container">
                                <i class="gg-sun"></i>
                                <i class="gg-moon"></i>
                            </div>
                        </label>
                    </div>
                </nav>
            </div>
            <!-- //toggle switch for light and dark theme -->
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-3 m-b-10">
                </div>
                <div class="col-md-5 m-b-10">
                    <!-- <marquee align="midden" onMouseOver="this.stop()" onMouseOut="this.start()">恭賀!</marquee> -->
                </div>
                <div class="col-md-4 m-b-10">
                </div>
            </div>
        </div>
    </div>
</header>