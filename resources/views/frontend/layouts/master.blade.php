<!--
Author: W3layouts
Author URL: http://w3layouts.com
-->
<!doctype html>
<html lang="zh-TW">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <link rel="icon" href="{!! asset('images/icon.png') !!}"/>
        <!-- google-fonts -->
        <link href="https:////fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,400;1,600;1,700&display=swap" rel="stylesheet">
        <!-- //google-fonts -->
        <!-- Template CSS -->
        <link rel="stylesheet" href="{{asset('css/style-starter.css')}}">
        <link rel="stylesheet" href="{{asset('css/float_menu.css')}}">
        <link rel="stylesheet" href="{{asset('css/keyframes.css')}}">
        <link rel="stylesheet" href="{{asset('css/out.css')}}">
        <link rel="stylesheet" href="{{asset('css/css-loader.css')}}">
    </head>
    <body>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

        @include('frontend.layouts.header')

        @yield('content')

        @include('frontend.layouts.footer')
        @include('frontend.layouts.copyright')
        @include('frontend.layouts.loader')
        <!-- common jquery plugin -->
        <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
        <!-- //common jquery plugin -->
        <!-- owl carousel for team -->
        <script src="{{asset('js/owl.carousel.js')}}"></script>
        <!-- theme switch js (light and dark)-->
        <script src="{{asset('js/theme-change.js')}}"></script>
        <!-- magnific popup -->
        <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <script>
            // When the user scrolls down 20px from the top of the document, show the button
            window.onscroll = function () {
                scrollFunction()
            };

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("movetop").style.display = "block";
                } else {
                    document.getElementById("movetop").style.display = "none";
                }
            }

            // When the user clicks on the button, scroll to the top of the document
            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }
            function scrollBlog(){
                document.body.scrollTop = $("#blog").offset().top;
                document.documentElement.scrollTop = $("#blog").offset().top;
            }
            $(document).ready(function () {
                $('.owl-carousel').owlCarousel({
                    loop: true,
                    margin: 0,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1,
                            nav: true
                        },
                        667: {
                            items: 2,
                            nav: true,
                            margin: 20
                        },
                        1000: {
                            items: 2,
                            nav: true,
                            loop: true,
                            margin: 20
                        }
                    }
                })
            });
            function autoType(elementClass, typingSpeed) {
                var thhis = $(elementClass);
                thhis.css({
                    "position": "relative",
                    "display": "inline-block"
                });
                thhis.prepend('<div class="cursor" style="right: initial; left:0;"></div>');
                thhis = thhis.find(".text-js");
                var text = thhis.text().trim().split('');
                var amntOfChars = text.length;
                var newString = "";
                thhis.text("|");
                setTimeout(function () {
                    thhis.css("opacity", 1);
                    thhis.prev().removeAttr("style");
                    thhis.text("");
                    for (var i = 0; i < amntOfChars; i++) {
                        (function (i, char) {
                            setTimeout(function () {
                                newString += char;
                                thhis.text(newString);
                            }, i * typingSpeed);
                        })(i + 1, text[i]);
                    }
                }, 1500);
            }

            $(document).ready(function () {
                // Now to start autoTyping just call the autoType function with the
                // class of outer div
                // The second paramter is the speed between each letter is typed.
                autoType(".type-js", 200);
            });
            $(document).ready(function () {
                $('.popup-with-zoom-anim').magnificPopup({
                    type: 'inline',

                    fixedContentPos: false,
                    fixedBgPos: true,

                    overflowY: 'auto',

                    closeBtnInside: true,
                    preloader: false,

                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                });

                $('.popup-with-move-anim').magnificPopup({
                    type: 'inline',

                    fixedContentPos: false,
                    fixedBgPos: true,

                    overflowY: 'auto',

                    closeBtnInside: true,
                    preloader: false,

                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-slide-bottom'
                });
            });
            $(window).on("scroll", function () {
                var scroll = $(window).scrollTop();

                if (scroll >= 80) {
                    $("#site-header").addClass("nav-fixed");
                } else {
                    $("#site-header").removeClass("nav-fixed");
                }
            });

            //Main navigation Active Class Add Remove
            $(".navbar-toggler").on("click", function () {
                $("header").toggleClass("active");
            });
            $(document).on("ready", function () {
                if ($(window).width() > 991) {
                    $("header").removeClass("active");
                }
                $(window).on("resize", function () {
                    if ($(window).width() > 991) {
                        $("header").removeClass("active");
                    }
                });
            });
            $(function () {
                $('.navbar-toggler').click(function () {
                    $('body').toggleClass('noscroll');
                });
            });
            setHeaderText();
            $(window).on("resize", function () {
                setHeaderText();
            });
            function setHeaderText(){
                if ($(window).width() > 991) {
                    $("#header_hello_ceo").css({
                        fontSize: 40
                    });
                }
                else{
                    $("#header_hello_ceo").css({
                        fontSize: 20
                    });
                }
            }
        </script>
        <script defer="defer">
            $('.loader-pokeball').hide();
        </script>
        <script defer="defer">
            $('a').on('click', function(event) {
                if ($(this).attr('class').indexOf('pass_load') < 0) {
                    $('.loader-pokeball').show();
                }
            });
            $('.loader-pokeball').hide();
        </script>
    </body>
</html>