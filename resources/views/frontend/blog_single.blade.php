<!-- {{ $last = $blog_block_count }} -->

@extends('frontend.layouts.master')

@section('title', __('index.blog_topic'))

@section('content')
<!-- inner banner -->
<div class="inner-banner">
  <section class="w3l-breadcrumb">
    <div class="container">
      <ul class="breadcrumbs-custom-path">
        <li><a href="{{ url('/') }}">{{ trans('index.blog_info') }}</a></li>
        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ $blog['title'] }}</li>
      </ul>
    </div>
  </section>
</div>
<!-- //inner banner -->
<!-- classes-section -->
<section class="w3l-index-block2 py-5">
  <div class="container py-md-4 py-3">
    <div class="title-heading-w3 text-center mx-auto">
      <h3 class="title-main">{{ $blog['title'] }}</h3>
      <img src="{{ asset($blog['photo']) }}" alt="" class="img-fluid" />
      <p style="">{!! nl2br(e($blog['short_introduction']), false) !!}</p>
      <strong class="fee-class-w3 mt-3">{!! nl2br(e($blog['content']), false) !!}</strong>
    </div>
    <div class="row bottom_grids mt-5 pt-lg-3">
      @foreach ( $blog_desc as $desc_key => $desc_val )
      <hr/>
      <div class="title-heading-w3 text-center mx-auto">
        <p style="">{!! nl2br(e($desc_val['content']), false) !!}</p>
      </div>
      @endforeach
    </div>
    <h3 class="title-main">{{ trans('index.blog_list') }}</h3>
    <div class="row bottom_grids mt-5 pt-lg-3">
      @foreach ( $blogs as $blogs_key => $blogs_val )
      <div class="col-lg-4 col-md-6 px-lg-4">
        <div class="s-block">
          <a href="{{ url('blog') }}/{{ $blogs_val['uuid'] }}" class="d-block">
            <img src="{{ asset($blogs_val['photo']) }}" alt="" class="img-fluid-class" />
            <div class="p-3">
              <h3 class="mb-2">{{ $blogs_val['title'] }}</h3>
              <p>{{ $blogs_val['short_introduction'] }}</p>
            </div>
          </a>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>
@endsection