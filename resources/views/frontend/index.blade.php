@extends('frontend.layouts.master')

@section('title', __('index.title'))

@section('content')

<!-- banner section -->
<section id="home" class="w3l-banner py-md-5 pt-md-0 pt-sm-5 pt-4">
    <div class="container py-lg-5 py-md-4 pt-md-0 pt-sm-1 mt-lg-0 mt-5">
        <div class="row align-items-center py-lg-5 py-md-5 mt-4">
            <div class="banner-image-w3 text-lg-center">
                <img width="500" src="{{ asset('images/index_hello_img.png') }}" alt="" class="img-fluid">
            </div>
            <div class="offset-lg-6 col-lg-6 offset-md-5 col-md-7 mt-lg-5 pt-md-4 pt-5">
                <h3 class="mb-sm-4 mb-3 hello-title">{{ trans('index.hello_title') }}<br>{{ trans('index.hello_ceo') }}
                    <span class="type-js"><span class="text-js">{{ trans('index.hello_ceo_move_word') }}</span></span></h3>
                <p class="paragraph-text">{{ trans('index.hello_info') }}</p>
                <div class="mt-md-5 mt-4 mb-lg-0 mb-4 hello-title">
                    <a class="btn btn-primary btn-style middle-align" href="{{ url('class') }}">{{ trans('index.hello_class_btn') }}</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //banner section -->
<!-- classes-section-1 -->
<section class="w3l-index-block2 py-5">
    <div class="container py-md-4 py-3">
        <div class="title-heading-w3 text-center mx-auto">
            <h3 class="title-main">{{ trans('index.classes_topic') }}</h3>
            <p class="mt-4 sub-title">{{ trans('index.classes_info') }}</p>
        </div>
        <div class="row bottom_grids mt-5 pt-lg-3">
            @foreach ( $classes as $classes_key => $class )
            <div class="col-lg-3 col-md-6 px-lg-2">
                <div class="s-block">
                    <a href="{{ url('class') }}/{{ $class['uuid'] }}" class="d-block">
                        <img src="{{ asset($class['photo']) }}" alt="" class="img-fluid-class" />
                        <div class="p-3">
                            <h3 class="mb-2">{{ $class['name'] }}</h3>
                            <p>{{ $class['introduction'] }}</p>
                            <strong class="fee-class-w3 mt-3">{{ $class['price'] }}</strong>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
        <div class="mt-5 mx-auto text-center">
            <a class="btn btn-primary btn-style" href="{{ url('class') }}">{{ trans('index.hello_all_view_class_btn') }}</a>
        </div>
    </div>
</section>
<!-- //classes-section-1 -->
<!-- 3 grids with icon section -->
<section class="w3l-feature-8">
    <div class="features-main py-5">
        <div class="container py-md-4 py-3">
            <div class="features">
                @foreach ( $visions as $vision_key => $vision )
                <div class="feature-{{ $vision_key+1 }}">
                    <div class="feature-body">
                        <div class="feature-images">
                            <span class="fa fa-bullhorn" aria-hidden="true"></span>
                        </div>
                        <br>
                        <div class="feature-info">
                            <a href="{{ $vision['link'] }}">
                                <h3 class="feature-titel">{{ $vision['title'] }}</h3>
                            </a>
                            <p class="feature-text">{{ $vision['short_introduction'] }}
                                <br>
                                <a href="{{ url('about') }}" class="feature-link">{{ trans('index.middle_read_more') }}<span
                                    class="arrow">&raquo;</span></a>
                            </p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- //3 grids with icon section -->
<!-- articles section -->
<section class="w3l-servicesblock py-5">
    <div class="container py-md-4 py-3">
        <div class="row">
            <div class="col-lg-6 left-wthree-img">
                <!-- float menu -->
                <ul id="menu">
                    <div class="anime">
                        <a class="pass_load" href="javascript:void(0);"><img class="photo-frame" src="{{ asset('images/advance1.jpg') }}" alt="#"></a>
                        <a class="pass_load" href="javascript:void(0);"><img class="photo-frame" src="{{ asset('images/advance2.jpg') }}" alt="#"></a>
                        <a class="pass_load" href="javascript:void(0);"><img class="photo-frame" src="{{ asset('images/advance3.jpg') }}" alt="#"></a>
                        <a class="pass_load" href="javascript:void(0);"><img class="photo-frame" src="{{ asset('images/advance4.jpg') }}" alt="#"></a>
                        <a class="pass_load" href="javascript:void(0);"><img class="photo-frame" src="{{ asset('images/advance5.jpg') }}" alt="#"></a>
                    </div>
                </ul>
                <!-- <img src="{{ asset('images/index_article.jpg') }}" alt="" class="img-fluid"> -->
            </div>
            <div class="col-lg-6 about-right-faq align-self mt-lg-0 mt-sm-5 mt-4 pl-lg-5">
                <h3 class="title-main-2 mb-3">{{ trans('index.article_topic') }}</h3>
                <div class="separatorhny"></div>
                <p class="">{{ trans('index.article_info') }}</p>

                <div class="row mt-md-5 mt-3">
                    @for ($i = 1; $i <= $articles_block; $i++)
                    <div class="col-sm-6 left-insp-art">
                        <ul>
                            @foreach ( $articles as $article_key => $article_arr )
                                @foreach ( $article_arr as $key => $article )
                                    @if ($article_key === $i)
                                    <li><i class="fa fa-hand-o-right mr-2" aria-hidden="true">
                                    </i>
                                    <a href="{{ url('article') }}/{{ $article['uuid'] }}">{{ $article['title'] }}</a>
                                    </li>
                                    @endif
                                @endforeach
                            @endforeach
                        </ul>
                    </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //articles section -->
<!-- team section -->
<section class="w3l-teams-1">
    <div class="teams1 py-5">
        <div class="container py-md-4 py-3">
            <div class="teams1-content">
                <div class="title-heading-w3 text-center mx-auto">
                    <h3 class="title-main">{{ trans('index.speaker_topic') }}</h3>
                    <p class="mt-4 sub-title">{{ trans('index.speaker_info') }}</p>
                </div>
                <div class="mt-5 pt-lg-4">
                    <div class="owl-carousel owl-theme">
                        @foreach ( $speakers as $speaker_key => $speaker )
                        <div class="item">
                            <div class="d-grid team-info">
                                <div class="column position-relative">
                                    <a href="{{ url('speaker') }}/{{ $speaker['uuid'] }}">
                                        <img class="full-height" src="{{ asset($speaker['photo']) }}" alt="" class="img-fluid" />
                                    </a>
                                    <h3 class="name-pos"><a href="{{ url('speaker') }}/{{ $speaker['uuid'] }}">{{ $speaker['name'] }}</a></h3>
                                </div>
                                <div class="column">
                                    <p>{{ $speaker['job_title'] }}</p>
                                    <div class="social">
                                        <a href="{{ $speaker['link_fb'] }}" class="facebook"><span class="fa fa-facebook"
                                                aria-hidden="true"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //team setion -->
<!-- blog section -->
<div class="w3l-new-block-6 py-5">
    <div class="container py-md-4 py-3">
        <div class="title-heading-w3 text-center mx-auto" id="blog">
            <h3 class="title-main">{{ trans('index.blog_topic') }}</h3>
            <p class="mt-4 sub-title">{{ trans('index.blog_info') }}</p>
        </div>
        <div class="d-grid mt-5 pt-lg-3">
            @foreach ( $blogs as $blog_key => $blog )
            <div class="grids5-info">
                <a href="{{ url('blog') }}/{{ $blog['uuid'] }}"><img src="{{ asset($blog['photo']) }}" alt="" class="img-fluid-class" /></a>
                <h4><a href="{{ url('blog') }}/{{ $blog['uuid'] }}">{{ $blog['blog_title'] }}</a></h4>
                <ul class=" admin-list">
                    <li><a href="#"><span class=" fa fa-user" aria-hidden="true"></span>by
                            {{ $blog['blog_owner'] }}</a></li>
                    <li><a href="#"><span class=" fa fa-comments-o" aria-hidden="true"></span>{{ $blog['blog_commit'] }} Comments</a></li>
                </ul>
                <p>{{ $blog['blog_desc'] }}</p>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- //blog section -->
<!-- middle section with full bg imgage -->
<div class="middle py-5">
    <div class="container py-md-5 py-4">
        <div class="welcome-left text-left py-lg-4">
            <h3>{{ trans('index.middle_banner_topic') }}</h3>
            <p class="mt-3 pr-lg-5 mr-lg-5">{{ trans('index.middle_banner_info') }}</p>
            <a href="{{ url('about') }}" class="btn btn-style btn-white mt-sm-5 mt-4 mr-2">{{ trans('index.middle_read_more') }}</a>
            <a href="{{ url('contact') }}" class="btn btn-style btn-primary mt-sm-5 mt-4">{{ trans('index.middle_contact_us') }}</a>
        </div>
    </div>
</div>
<!-- //middle section with full bg imgage -->
<!-- testimonial section -->
    <div class="w3l-cutomer-main-cont">
        <div class="testimonials text-center py-5">
            <div class="container py-md-5 py-4">
                <div class="title-heading-w3 text-center mx-auto">
                    <h3 class="title-main">{{ trans('index.middle_customers_topic') }}</h3>
                    <p class="mt-4 sub-title"> {{ trans('index.middle_customers_info') }}</p>
                </div>
                <div class="row content-sec mt-md-5 mt-4">
                    @foreach ( $customers as $customers_key => $customer )
                    <div class="col-lg-4 col-md-6 testi-sections">
                        <div class="testimonials_grid">
                            <p class="sub-test"><span class="fa fa-quote-left mr-2" aria-hidden="true"></span> {{ $customer['message'] }}
                            </p>
                            <div class="d-grid sub-author-con">
                                <div class="testi-img-res">
                                    <img src="{{ asset($customer['photo']) }}" alt="" class="img-fluid" />
                                </div>
                                <div class="testi_grid text-left">
                                    <h5>{{ $customer['name'] }}</h5>
                                    <p>{{ $customer['job_title'] }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
<!-- //testimonial section -->
<!-- newsletter section -->
<section class="w3l-form-26">
    <div class="form-26-main">
        <div class="container py-md-5">
            <div class="form-inner-cont">
                <div class="title-heading-w3 text-center mx-auto">
                    <h3 class="title-main">{{ trans('index.newsletter_topic') }}</h3>
                    <p class="mt-4 pt-2 sub-title">{{ trans('index.newsletter_info') }}</p>
                </div>
                <div class="form-right-inf mt-5">
                    <form action="#" method="post" class="signin-form">
                        <div class="forms-gds">
                            <div class="form-input">
                                <input type="email" autocomplete="off" name="email" placeholder="{{ trans('index.email_info') }}" required />
                            </div>
                            <div class="btn btn-style btn-primary button-eff-news">
                                <button onclick="javascript:location.href='{{ url('contact') }}'" class="btn">{{ trans('index.subscribe') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- newsletter image -->
        <div class="png-img-w3ls">
            <!-- <img src="{{ asset('images/index_hello_img_footer.jpg') }}" width="120" alt="" class="img-responsive"> -->
        </div>
        <!-- //newsletter image -->
    </div>
</section>
<!-- //newsletter section -->
@endsection
