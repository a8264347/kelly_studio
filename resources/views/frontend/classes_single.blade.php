<!-- {{ $last = $classes_block_count }} -->

@extends('frontend.layouts.master')

@section('title', __('classes.title_desc'))

@section('content')
<!-- inner banner -->
<div class="inner-banner">
  <section class="w3l-breadcrumb">
    <div class="container">
      <ul class="breadcrumbs-custom-path">
        <li><a href="{{ url('class') }}">{{ trans('classes.classes') }}</a></li>
        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ $class['name'] }}</li>
      </ul>
    </div>
  </section>
</div>
<!-- //inner banner -->
<!-- classes-section -->
<section class="w3l-index-block2 py-5">
  <div class="container py-md-4 py-3">
    <div class="title-heading-w3 text-center mx-auto">
      <h3 class="title-main">{{ $class['name'] }}</h3>
      <img src="{{ asset($class['photo']) }}" alt="" class="img-fluid" />
      <p>{{ $class['introduction'] }}</p>
      <strong class="fee-class-w3 mt-3">{!! nl2br(e($class['content']), false) !!}</strong>
      <hr>
      <strong class="fee-class-w3 mt-3">{{ $class['price'] }}</strong>
    </div>
    <div class="col-lg-12 col-md-12 px-lg-12">
      <div class="p-3"></div>
    </div>
    <h3 class="title-main">{{ trans('classes.topic') }}</h3>
    @for ($i = 1; $i <= $last; $i++)
    <div class="row bottom_grids mt-5 pt-lg-3">
      @foreach ( $classes as $classes_key => $classe )
      <div class="col-lg-3 col-md-6 px-lg-2">
        <div class="s-block">
          <a href="{{ url('class') }}/{{ $classe['uuid'] }}" class="d-block">
            <img src="{{ asset($classe['photo']) }}" alt="" class="img-fluid-class" />
            <div class="p-3">
              <h3 class="mb-2">{{ $classe['name'] }}</h3>
              <p>{{ $classe['introduction'] }}</p>
              <strong class="fee-class-w3 mt-3">{{ $classe['price'] }}</strong>
            </div>
          </a>
        </div>
      </div>
      @endforeach
    </div>
    @endfor
  </div>
</section>
@endsection