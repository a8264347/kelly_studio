@extends('frontend.layouts.master')

@section('title', __('about.title'))

@section('content')
  <!-- inner banner -->
  <div class="inner-banner">
    <section class="w3l-breadcrumb">
      <div class="container">
        <ul class="breadcrumbs-custom-path">
          <li><a href="{{ url('/') }}">{{ trans('about.home') }}</a></li>
          <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span> {{ trans('about.about_us') }}</li>
        </ul>
      </div>
    </section>
  </div>
  <!-- //inner banner -->
  <!-- about section -->
  <section class="w3l-text-6 py-5" id="about">
    <div class="text-6-mian py-md-5">
      <div class="container">
        <div class="row top-cont-grid align-items-center">
          <div class="col-lg-6 left-img pr-lg-4">
            <ul id="menu">
                <div class="anime">
                    <a class="pass_load" href="javascript:void(0);"><img class="photo-frame" src="{{ asset('images/advance1.jpg') }}" alt="#"></a>
                    <a class="pass_load" href="javascript:void(0);"><img class="photo-frame" src="{{ asset('images/advance2.jpg') }}" alt="#"></a>
                    <a class="pass_load" href="javascript:void(0);"><img class="photo-frame" src="{{ asset('images/index_article.jpg') }}" alt="#"></a>
                    <a class="pass_load" href="javascript:void(0);"><img class="photo-frame" src="{{ asset('images/promote1.png') }}" alt="#"></a>
                    <a class="pass_load" href="javascript:void(0);"><img class="photo-frame" src="{{ asset('images/promote2.png') }}" alt="#"></a>
                </div>
            </ul>
            <!-- <img src="assets/images/about.jpg" alt="" class="img-responsive img-fluid" /> -->
          </div>
          <div class="col-lg-6 text-6-info mt-lg-0 mt-4">
            <h6>{{ trans('about.topic') }}</h6>
            <h2>{{ trans('about.info') }} <span>{{ trans('about.hello_ceo') }}</span></h2>
            <p>{{ trans('about.welcome_info') }}</p>
            <a href="{{ url('contact') }}" class="btn btn-style btn-primary mt-sm-5 mt-4">{{ trans('about.read_more') }}</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- //about section -->
  <!-- stats -->
  <section class="w3_stats py-5" id="stats">
    <div class="container py-md-4">
      <div class="title-heading-w3 text-center mx-auto">
        <h3 class="title-main">{{ trans('about.statistics_topic') }}</h3>
        <p class="mt-4 sub-title"> {{ trans('about.statistics_info') }}</p>
      </div>
      <div class="w3-stats text-center mt-5">
        <div class="row">
          <div class="col-md-3 col-6">
            <div class="counter">
              <span class="fa fa-users"></span>
              <div class="timer count-title count-number mt-3" data-to="{{ $instructors }}" data-speed="1500"></div>
              <p class="count-text">{{ trans('about.instructors_desc') }}</p>
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="counter">
              <span class="fa fa-video-camera"></span>
              <div class="timer count-title count-number mt-3" data-to="{{ $views }}" data-speed="1500"></div>
              <p class="count-text ">{{ trans('about.views_desc') }}</p>
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="counter">
              <span class="fa fa-smile-o"></span>
              <div class="timer count-title count-number mt-3" data-to="{{ $students }}" data-speed="1500"></div>
              <p class="count-text">{{ trans('about.happy_student_desc') }}</p>
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="counter">
              <span class="fa fa-thumbs-up"></span>
              <div class="timer count-title count-number mt-3" data-to="{{ $statisfication }}" data-speed="1500"></div>
              <p class="count-text">{{ trans('about.statisfication_desc') }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- //stats -->
  <!-- team section -->
  <section class="w3l-teams-1">
    <div class="teams1 py-5">
      <div class="container py-md-4 py-3">
        <div class="teams1-content">
          <div class="title-heading-w3 text-center mx-auto">
            <h3 class="title-main">{{ trans('index.speaker_topic') }}</h3>
            <p class="mt-4 sub-title"> {{ trans('index.speaker_info') }}</p>
          </div>
          <div class="mt-5 pt-lg-4">
            <div class="owl-carousel owl-theme">
              @foreach ( $speakers as $speaker_key => $speaker )
              <div class="item">
                  <div class="d-grid team-info">
                      <div class="column position-relative">
                          <a href="{{ url('speaker') }}/{{ $speaker['uuid'] }}"><img class="full-height" src="{{ asset($speaker['photo']) }}" alt=""
                                  class="img-fluid" /></a>
                          <h3 class="name-pos"><a href="{{ url('speaker') }}/{{ $speaker['uuid'] }}">{{ $speaker['name'] }}</a></h3>
                      </div>
                      <div class="column">
                          <p>{{ $speaker['job_title'] }}</p>
                          <div class="social">
                              <a href="{{ $speaker['link_fb'] }}" class="facebook"><span class="fa fa-facebook"
                                      aria-hidden="true"></span></a>
                          </div>
                      </div>
                  </div>
              </div>
              @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- //team setion -->
  <!-- middle section with full bg imgage -->
  <div class="middle py-5">
    <div class="container py-md-5 py-4">
        <div class="welcome-left text-left py-lg-4">
            <h3>{{ trans('index.middle_banner_topic') }}</h3>
            <p class="mt-3 pr-lg-5 mr-lg-5">{{ trans('index.middle_banner_info') }}</p>
            <a href="{{ url('about') }}" class="btn btn-style btn-white mt-sm-5 mt-4 mr-2">{{ trans('index.middle_read_more') }}</a>
            <a href="{{ url('contact') }}" class="btn btn-style btn-primary mt-sm-5 mt-4">{{ trans('index.middle_contact_us') }}</a>
        </div>
    </div>
  </div>
  <!-- //middle section with full bg imgage -->
  <!-- testimonial section -->
  <div class="w3l-cutomer-main-cont">
      <div class="testimonials text-center py-5">
          <div class="container py-md-5 py-4">
              <div class="title-heading-w3 text-center mx-auto">
                  <h3 class="title-main">{{ trans('index.middle_customers_topic') }}</h3>
                  <p class="mt-4 sub-title"> {{ trans('index.middle_customers_info') }}</p>
              </div>
              <div class="row content-sec mt-md-5 mt-4">
                  @foreach ( $customers as $customers_key => $customer )
                    <div class="col-lg-4 col-md-6 testi-sections">
                        <div class="testimonials_grid">
                            <p class="sub-test"><span class="fa fa-quote-left mr-2" aria-hidden="true"></span> {{ $customer['message'] }}
                            </p>
                            <div class="d-grid sub-author-con">
                                <div class="testi-img-res">
                                    <img src="{{ asset($customer['photo']) }}" alt="" class="img-fluid" />
                                </div>
                                <div class="testi_grid text-left">
                                    <h5>{{ $customer['name'] }}</h5>
                                    <p>{{ $customer['job_title'] }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                  @endforeach
              </div>
          </div>
      </div>
  </div>
  <!-- //testimonial section -->
@endsection