<!-- {{ $last = $article_block_count }} -->

@extends('frontend.layouts.master')

@section('title', __('article.title_desc'))

@section('content')
<!-- inner banner -->
<div class="inner-banner">
  <section class="w3l-breadcrumb">
    <div class="container">
      <ul class="breadcrumbs-custom-path">
        <li><a href="{{ url('/') }}">{{ trans('article.name') }}</a></li>
        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ $article['title'] }}</li>
      </ul>
    </div>
  </section>
</div>
<!-- //inner banner -->
<!-- classes-section -->
<section class="w3l-index-block2 py-5">
  <div class="container py-md-4 py-3">
    <div class="title-heading-w3 text-center mx-auto">
      <h3 class="title-main">{{ $article['title'] }}</h3>
      <img src="{{ asset($article['photo']) }}" alt="" class="img-fluid" />
      <strong class="fee-class-w3 mt-3">{!! nl2br(e($article['content']), false) !!}</strong>
      <hr>
    </div>
    <div class="row bottom_grids mt-5 pt-lg-3">
      @foreach ( $article_desc as $article_key => $article_val )
      <div class="title-heading-w3 text-center mx-auto">
        <img src="{{ asset($article_val['photo']) }}" style="{{ $article_val['type_first'] }}" alt="" class="img-fluid" />
        <p style="{{ $article_val['type_first'] }}">{{ $article_val['photo_desc'] }}</p>
        <br>
        <strong class="fee-class-w3 mt-3">{!! nl2br(e($article_val['content']), false) !!}</strong>
        <br>
        <img src="{{ asset($article_val['photo']) }}" style="{{ $article_val['type_second'] }}" alt="" class="img-fluid" />
        <p style="{{ $article_val['type_second'] }}">{{ $article_val['photo_desc'] }}</p>
      </div>
      @endforeach
    </div>
  </div>
</section>
@endsection