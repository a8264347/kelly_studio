<!DOCTYPE html>
<html>
	<head>
	  	<meta charset="utf-8">
	  	<style>
	    	body {
	      		text-align: center;
	    	}
	    	header {
	      		margin-bottom: 1rem;
	    	}
	  	</style>
	</head>
	<body>
  		<header>
    		{{ $params['title'] }}
  		</header>
  		<main>
  			@foreach ( $params['content'] as $main_key => $main )
            	{{ $main }}<br>
            @endforeach
  		</main>
	</body>
</html>