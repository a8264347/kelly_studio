<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contact Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'title'      => 'Kelly Studio | Kelly Studio Contact Us',
    'home'       => 'Home',
    'contact_us' => 'Contact Us',
    'interest'   => 'interest class',
    'topic'      => 'Get In Touch',
    'info'       => 'Really Happy To See You , Call Us.',
    'address_topic' => 'Address:',
    'address'       => '4F., No. 358, Yucheng Rd., Zuoying Dist., Kaohsiung City 813, Taiwan (R.O.C.)',
    'address_email' => 'Email:',
    'email'         => 't3472120@yahoo.com.tw',
    'address_phone' => 'Phone:',
    'phone'         => '0973239337',
    'form_name'     => 'Name',
    'form_email'    => 'Email',
    'form_phone'    => 'Phone Number',
    'form_msg'      => 'Message',
    'submit'        => 'Submit Now'
];
