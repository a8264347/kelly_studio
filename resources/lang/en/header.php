<?php

return [

    /*
    |--------------------------------------------------------------------------
    | header Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */

    'hello_ceo'         => 'Kelly Studio',
    'home'              => 'Home',
    'about_us'          => 'About us',
    'classes'           => 'Classes',
    'contact_us'        => 'Contact Us',
    'keyword'           => 'Enter Keyword'
];
