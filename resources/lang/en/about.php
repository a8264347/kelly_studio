<?php

return [

    /*
    |--------------------------------------------------------------------------
    | About Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'title'     => 'Kelly Studio | About Kelly Studio',
    'home'      => 'Home',
    'about_us'  => 'About Us',
    'topic'     => 'ABOUT OUR SITE',
    'info'      => 'Welcome to our class',
    'hello_ceo' => 'Kelly studio',
    'welcome_info'  => 'Nice To See You And Congratulations You Are On The Buety Way',
    'read_more'     => 'Read More',
    'statistics_topic'  => 'Our Statistics',
    'statistics_info'   => 'We Will Take Care Our Student In Any Time',
    'instructors_desc'  => 'Awesome Instructors',
    'views_desc'        => 'Class Views',
    'happy_student_desc'    => 'Happy Students',
    'statisfication_desc'   => 'Statisfication'
];
