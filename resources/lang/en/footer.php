<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Index Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'hello_ceo'     => 'Kelly Studio',
    'msg'           => 'Kelly teaches you how to get the certificate, Join our Amazing online classes',
    'link_topic'    => 'Quick Links',
    'about_us'      => 'About Us',
    'classes'       => 'classes',
    'blog'          => 'Blog Posts',
    'more_info'     => 'More Info',
    'privacy_policy'=> 'Privacy Policy',
    'contact_us'    => 'Contact us',
    'contact_info'  => 'Contact Info',
    'address'       => '4F., No. 358, Yucheng Rd., Zuoying Dist., Kaohsiung City 813, Taiwan (R.O.C.)',
    'phone'         => '0973239337',
    'email'         => 't3472120@yahoo.com.tw'
];
