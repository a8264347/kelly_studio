<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Classes Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'success'       => 'Success',
    'warning'       => 'Warning',
    'error'         => 'Error',
    'success_msg'   => "We will send the email back! thank you!",
    'warning_msg'   => "Ops! Maybe something is missing! Please check the field!",
    'error_msg'     => "This form is broken or your message is wrong! Please wait for second!",
    'info'          => "Are you sure to send the form?",
    'cancel'        => "Cancel",
    'confirm'       => "Confirm",
    'email'         => 'The email must be a valid',
    'name'          => 'The name must be a valid',
    'phone'         => 'The phone must be a valid',
    'message'       => 'The remark must be a valid',
    'class_id'      => 'The class must be a valid',
    'google_recaptcha_token' => 'The bot must be a valid'
];
