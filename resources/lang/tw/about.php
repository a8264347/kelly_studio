<?php

return [

    /*
    |--------------------------------------------------------------------------
    | About Language Lines TW
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'title'     => '整體美時尚創意協會 | 關於整體美',
    'home'      => '整體美時尚首頁',
    'about_us'  => '關於我們',
    'topic'     => '與我們的網站',
    'info'      => '歡迎來到整體美時尚創意協會',
    'hello_ceo' => '',
    'welcome_info'  => '美容美髮丙乙級課程招生中，歡迎即刻報名',
    'read_more'     => '了解更多',
    'statistics_topic'  => '成果展現/榮譽榜',
    'statistics_info'   => '只有不肯學，沒有學不會',
    'instructors_desc'  => '師資陣容',
    'views_desc'        => '課程總覽',
    'happy_student_desc'    => '學員見證',
    'statisfication_desc'   => '課程滿意度'
];
