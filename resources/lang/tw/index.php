<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Index Language Lines TW
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'title'           => '整體美時尚創意協會 | 美容專業證照、美容丙乙級',
    'hello_title'     => '歡迎來到整體美時尚創意協會',
    'hello_ceo'       => '專業近在',
    'hello_ceo_move_word' => '你手',
    'hello_info'      => '專業證照在手，創業不是夢 ! Kelly 老師在美容界深耕20年，專業講師授課，培訓人才，證照輔導班、創業輔導班，不遺餘力、因材施教的 Kelly 老師，其輔導學生之考照通過率為 100%',
    'hello_class_btn' => '快速瀏覽',
    'classes_topic'   => '熱門課程',
    'classes_info'    => '打造專業與美麗的巔峰',
    'hello_all_view_class_btn' => '盡速瀏覽所有課程',
    'article_topic'   => '豐富與精采教學文章',
    'article_info'    => '只挑選適合你的，如果你喜歡，歡迎來電洽詢。',
    'speaker_topic'   => '整體美時尚創意協會 | 美容、美甲、芳療講師',
    'speaker_professional' => '專業講師',
    'speaker_info'    => '讓他們的手腕，帶領著一起朝向美麗的向陽',
    'blog_topic'      => '整體美時尚創意協會 | 美容部落格',
    'blog_info'       => '近期精彩的部落格分享',
    'blog_list'       => '其他部落格',
    'middle_banner_topic' => '擁有美麗與專業兼具的你',
    'middle_banner_info'  => '離入門之路，只差一步之遙。',
    'middle_read_more'    => '想知道更多',
    'middle_contact_us'   => '聯繫我們',
    'middle_customers_topic' => '學員/客戶見證',
    'middle_customers_info'  => '有關於那些年我們一起打造美貌的傑帥與仙女們',
    'newsletter_topic'    => '訂閱我們的課程',
    'newsletter_info'     => '讓我們知道，你需要經濟又實惠的專業課程',
    'subscribe'           => '訂閱',
    'email_info'          => '請輸入您的電子郵件'
];
