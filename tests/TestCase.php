<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use JsonSchema\Validator;
use Mockery;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function initMock($class)
    {
        $mock = Mockery::mock($class);
        $this->app->instance($class, $mock);

        return $mock;
    }

	public function signIn($data=['email'=>'youremail@yahoo.com', 'password'=>'password'])
	{
	    $this->post('/api/login', $data);
	    $content = json_decode($this->response->getContent());

	    $this->assertObjectHasAttribute('token', $content, 'Token does not exists');
	    $this->token = $content->token;
	    // $headers['Authorization'] = 'Bearer ' . $token;
	    // $this->call('GET', '/restricted/page', ['token'=>$this->token], $cookies = [], $files = [], $server = []);
	    return $this;
	}

	public function assertJsonStringValidatedAgainstJsonSchemaFile($expectedJsonSchemaFile, $actualJson, $baseUri = __DIR__ . '/json')
    {
        $schemaUri = sprintf('%s/%s', $baseUri, $expectedJsonSchemaFile);
        $schema = json_decode(file_get_contents($schemaUri));
        $json = $actualJson;
        if (is_string($actualJson)) {
            $json = json_decode($actualJson);
        }

        $validator = new Validator();
        $validator->validate($json, $schema);
        $this->assertTrue($validator->isValid(), json_encode($validator->getErrors(), JSON_PRETTY_PRINT));
    }

    public function mockTest($expected,$use_function,$correct_data){
        $mock = $this->initMock(ProductMenu::class);

        $mock->shouldReceive($use_function)
            ->once()
            ->andReturn($expected);

        $this->assertEquals($expected, $correct_data);

        Mockery::close();
    }
}
