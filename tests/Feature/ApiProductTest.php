<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\ProductMenu;
use App\Product;

class ApiProductTest extends TestCase
{
    use WithoutMiddleware;

    public function setUp() : void
    {
        parent::setUp();
        putenv('APP_ENV=develop');
    }

    public function testGetProductAPI()
    {
        /** act */
        $response = $this->call('GET', '/api/auth/product');
        $result = json_decode($response->getContent(), true);
        /** assert **/
        $this->assertEquals(true, $result['success']);
        $this->assertJsonStringValidatedAgainstJsonSchemaFile('product.json', $response->getContent());
    }
}