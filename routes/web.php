<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/lang/{locale}', 'LocalizationController@switchLang');
//view
Route::get('/', 'IndexHomeController@index')->name('index');
Route::get('about', 'IndexAboutController@index')->name('about');
Route::get('class', 'IndexClassesController@index')->name('class');
Route::get('class/{uuid}', 'IndexClassesController@show')->name('class_single');
Route::get('blog', 'IndexBlogController@index')->name('blog');
Route::get('contact', 'IndexContactController@index')->name('contact');
Route::post('contact_form', 'IndexContactController@formStore')->name('contact_form');
Route::get('/article/{uuid}', 'ArticleDescController@showArticlesById');
Route::get('/speaker/{uuid}', 'SpeakerController@showSpeakersById');
Route::get('/blog/{uuid}', 'BlogController@showBlogsById');

//data
$router->group(['prefix' => 'api','middleware' => ['cors', 'log.studio']], function ($router) {
	Route::post('login', 'AuthController@login');
	Route::post('logout', 'AuthController@logout');
	Route::post('refresh', 'AuthController@refresh');
	$router->group(['prefix' => 'auth','middleware' => 'auth.jwt'], function ($router) {
		Route::get('me', 'AuthController@me');
		Route::get('/speaker', 'SpeakerController@index');
		Route::get('/speaker/{uuid}', 'SpeakerController@show');
		Route::get('/article', 'ArticleController@index');
		Route::get('/article/{uuid}', 'ArticleController@show');
		Route::get('/blog', 'BlogController@index');
		Route::get('/blog/{uuid}', 'BlogController@show');
		Route::get('/blog_msg/{id}', 'BlogMsgController@index');
		Route::get('/blog_msg/{uuid}', 'BlogMsgController@show');
		Route::get('/classes', 'ClassesController@index');
		Route::get('/classes/{uuid}', 'ClassesController@show');
		Route::get('/slogan', 'SloganController@index');
		Route::get('/slogan/{uuid}', 'SloganController@show');
		Route::get('/customer', 'CustomerController@index');
		Route::get('/customer/{uuid}', 'CustomerController@show');
		Route::post('/applicationAuth', 'ApplicationAuthController@create');

		Route::apiResource('/product', 'ProductController');
		Route::post('product/restore/{id}', 'ProductController@productRestore');
	});
});
