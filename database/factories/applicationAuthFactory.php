<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ApplicationAuth;

$factory->define(Article::class, function (Faker\Generator $faker) {
    $zhFaker = Faker\Factory::create('zh_TW');
    return [
        'company_name'  => $zhFaker->company(50),
        'company_name_e'=> $faker->company(50),
        'username' 		=> $faker->name,
        'password' 		=> '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',// secret
        'email'			=> $faker->unique()->safeEmail,
        'ip' 			=> $faker->ipv4,
        'url' 			=> $faker->url,
        'accept_by'  	=> rand(0, 4)
    ];
});
