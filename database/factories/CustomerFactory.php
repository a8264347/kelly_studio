<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Customer;

$factory->define(Customer::class, function (Faker\Generator $faker) {
    $zhFaker = Faker\Factory::create('zh_TW');
    return [
        'name' => $zhFaker->name,
        'name_e' => $faker->name,
        'job_title' => $zhFaker->jobTitle(20),
        'job_title_e' => $faker->jobTitle(50),
        'photo' => 'https://picsum.photos/500',
        'introduction' => $zhFaker->text(100),
        'introduction_e' => $faker->text(100),
        'message' => $zhFaker->text,
        'class_id' => rand(0, 4),
        'created_by' => rand(0, 4),
        'updated_by'  => rand(0, 4)
    ];
});
