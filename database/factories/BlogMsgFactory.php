<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BlogMsg;

$factory->define(BlogMsg::class, function (Faker\Generator $faker) {
	$zhFaker = Faker\Factory::create('zh_TW');
    return [
    	'blog_id' => rand(0, 4),
    	'content' => $zhFaker->text,
    	'content_e' => $faker->text,
        'created_by' => rand(0, 4),
        'updated_by'  => rand(0, 4)
    ];
});
