<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Slogan;

$factory->define(Slogan::class, function (Faker\Generator $faker) {
    $zhFaker = Faker\Factory::create('zh_TW');
    return [
        'title' => $zhFaker->jobTitle,
        'title_e' => $faker->jobTitle,
        'short_introduction' => $zhFaker->text(100),
        'short_introduction_e' => $faker->text(100),
        'mark_class' => 'fa-bullhorn',
        'content' => $zhFaker->text,
        'content_e' => $faker->text,
        'created_by' => rand(0, 4),
        'updated_by'  => rand(0, 4)
    ];
});
