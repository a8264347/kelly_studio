<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ArticleDesc;

$factory->define(ArticleDesc::class, function (Faker\Generator $faker) {
	$zhFaker = Faker\Factory::create('zh_TW');
    return [
    	'article_id'=> factory('App\Article')->create()->id,
        'content'   => $zhFaker->text,
        'content_e' => $faker->text,
        'photo' => 'https://picsum.photos/500',
        'photo_desc' => '圖'.rand(1, 4).'-'.rand(1, 4),
        'type'	=> rand(0, 1),
        'created_by' => rand(0, 4),
        'updated_by' => rand(0, 4)
    ];
});
