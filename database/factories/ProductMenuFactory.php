<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProductMenu;

$factory->define(ProductMenu::class, function (Faker\Generator $faker) {
	$zhFaker = Faker\Factory::create('zh_TW');
    return [
    	'name' => $zhFaker->name,
        'name_e' => $faker->name,
        'photo' => 'https://picsum.photos/500'
    ];
});
