<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;

$factory->define(Article::class, function (Faker\Generator $faker) {
    $zhFaker = Faker\Factory::create('zh_TW');
    return [
        'title' => $zhFaker->jobTitle(5),
        'title_e' => $faker->jobTitle(5),
        'content' => $zhFaker->text,
        'content_e' => $faker->text,
        'article_type'	=> rand(0, 4),
        'photo' => 'https://picsum.photos/500',
        'created_by' => rand(0, 4),
        'updated_by' => rand(0, 4)
    ];
});
