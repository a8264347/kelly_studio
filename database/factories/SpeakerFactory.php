<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Speaker;

$factory->define(Speaker::class, function (Faker\Generator $faker) {
	$zhFaker = Faker\Factory::create('zh_TW');
    return [
        'name' => $zhFaker->name,
        'name_e' => $faker->name,
        'job_title' => $zhFaker->jobTitle(50),
        'job_title_e' => $faker->jobTitle(50),
        'speakers_type' => rand(0, 4),
        'photo' => 'https://picsum.photos/500',
        'introduction' => $zhFaker->text(100),
        'introduction_e' => $faker->text(100),
        'content' => $zhFaker->text,
        'content_e' => $faker->text,
        'link_fb' => $faker->url,
        'created_by' => rand(0, 4),
        'updated_by'  => rand(0, 4)
    ];
});
