<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blog;

$factory->define(Blog::class, function (Faker\Generator $faker) {
	$zhFaker = Faker\Factory::create('zh_TW');
    return [
        'title' => $zhFaker->jobTitle(50),
        'title_e' => $faker->jobTitle(50),
        'short_introduction' => $zhFaker->text(100),
        'short_introduction_e' => $faker->text(100),
        'content' => $zhFaker->text,
        'content_e' => $faker->text,
        'photo' => 'https://picsum.photos/500',
        'created_by' => rand(0, 4),
        'updated_by'  => rand(0, 4)
    ];
});
