<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Classes;

$factory->define(Classes::class, function (Faker\Generator $faker) {
    $zhFaker = Faker\Factory::create('zh_TW');
    return [
        'name' => $zhFaker->name,
        'name_e' => $faker->name,
        'classes_type' => rand(0, 4),
        'price' => rand(1000, 90000),
        'photo' => 'https://picsum.photos/500',
        'introduction' => $zhFaker->text(100),
        'introduction_e' => $faker->text(100),
        'content' => $zhFaker->text,
        'content_e' => $faker->text,
        'speaker_id' => rand(0, 4),
        'created_by' => rand(0, 4),
        'updated_by'  => rand(0, 4)
    ];
});
