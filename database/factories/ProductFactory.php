<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;

$factory->define(Product::class, function (Faker\Generator $faker) {
	$zhFaker = Faker\Factory::create('zh_TW');
	static $order = 1;
    return [
    	'name' => $zhFaker->name,
        'name_e' => $faker->name,
        'photo' => 'https://picsum.photos/500',
        'price' => rand(1000, 90000),
        'introduction' => $zhFaker->text(100),
        'introduction_e' => $faker->text(100),
        'product_menu_id' => $order++
    ];
});
