<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationAuthTalbe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_auth', function (Blueprint $table) {
            $table->bigIncrements('id',20);
            $table->uuid('uuid')->index();
            $table->string('company_name', 50);
            $table->char('company_name_e', 50);
            $table->string('username', 50);
            $table->string('password');
            $table->string('email')->unique();
            $table->string('ip', 50);
            $table->string('url');
            $table->bigInteger('accept_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_auth');
    }
}
