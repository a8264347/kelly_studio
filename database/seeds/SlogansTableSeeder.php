<?php

use Illuminate\Database\Seeder;

class SlogansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Slogan', 10)->create();
    }
}
