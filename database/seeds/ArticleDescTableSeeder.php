<?php

use Illuminate\Database\Seeder;

class ArticleDescTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\ArticleDesc', 10)->create();
    }
}
