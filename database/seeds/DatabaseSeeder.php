<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SpeakersTableSeeder::class);
        $this->call(BlogsTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(ClassesTableSeeder::class);
        $this->call(ArticlesTableSeeder::class);
        $this->call(ArticleDescTableSeeder::class);
        $this->call(BlogMsgsTableSeeder::class);
        $this->call(SlogansTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(ProductMenuTableSeeder::class);
    }
}
