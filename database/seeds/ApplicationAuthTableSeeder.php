<?php

use Illuminate\Database\Seeder;

class ApplicationAuthTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\ApplicationAuth', 1)->create();
    }
}
