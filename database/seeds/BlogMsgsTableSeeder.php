<?php

use Illuminate\Database\Seeder;

class BlogMsgsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\BlogMsg', 10)->create();
    }
}
