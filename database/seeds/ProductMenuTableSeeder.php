<?php

use Illuminate\Database\Seeder;

class ProductMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\ProductMenu', 10)->create();
    }
}
