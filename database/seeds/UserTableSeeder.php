<?php

use Illuminate\Database\Seeder;

use App\User;

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'name'     => 'edmond',
            'email'    => 'edmond@mail.com',
            'password' => Hash::make('password'),
        ]);
    }
}